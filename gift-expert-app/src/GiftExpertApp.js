import React, {useState} from 'react'
import { AddCategory } from './components/AddCategory';

export const GiftExpertApp = () => {

    //const categories = ['Pokemon', 'Naruto', 'Dragon Ball Z'];

    const [categories, setCategories] = useState(['Pokemon', 'Naruto', 'Dragon Ball Z'])

    /*
    const handleAdd = () => {
        //setCategories( [...categories, 'Yu-gi-oh!'] );
        setCategories( cats => [...cats, 'Yu-gi-oh!'] );
    }
    */

    return (
        <>
            <h2>GiftExpertApp</h2>
            <AddCategory />
            <hr />
            <ul>
                {
                    categories.map(categorie => {
                        return <li key={categorie} >{categorie}</li>;
                    })
                }
            </ul>

        </>
    )
}
